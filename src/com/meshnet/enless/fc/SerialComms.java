package com.meshnet.enless.fc;

import com.google.common.primitives.Ints;
import static com.meshnet.enless.fc.TrendConfig.systemLogArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 *
 * @author G Bhogal
 */
public class SerialComms {

    static final Logger logger = LogManager.getLogger(SerialComms.class.getName());
    private SerialPort serialPort;
    String portIdentifier = "";
    public static Timer serialPortWriteTimer;
    static int serialPortWriteRetryCount = 0;
    static ArrayList<Integer> completePacketAsList;// USED FOR WRITING DATA TO THE SERIAL PORT

    /**
     *
     * @param portId
     * @param baudRate
     * @param bits
     * @param parity
     * @param stopBits
     */
    public SerialComms(String portId, int baudRate, int bits, String parity, int stopBits) {

        completePacketAsList = new ArrayList<>();
        completePacketAsList.add(255); // ADD ONE ELEMENT SO THAT IT CAN BE REMOVED BELOW
        serialPortWriteTimer = new Timer(1000, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent evt) {

                sleep(51);//

                // REPEAT 7 TIMES ONLY THEN STOP
                if (serialPortWriteRetryCount < 7) {
                    logger.debug("serialPortWriteTimer Timer");

                    if (!completePacketAsList.isEmpty()) {
                        serialPortWrite(completePacketAsList);
                    }
                    serialPortWriteRetryCount++;

                } else {
                    serialPortWriteTimer.stop();
                    serialPortWriteRetryCount = 0;
                }
            }
        });
        serialPortWriteTimer.setInitialDelay(1000);
        serialPortWriteTimer.setRepeats(true);

        portIdentifier = portId;
        // PARITY CHECK
        int parityInteger = SerialPort.PARITY_NONE;

        if (parity.equalsIgnoreCase("N")) {
            parityInteger = SerialPort.PARITY_NONE;
        }
        if (parity.equalsIgnoreCase("E")) {
            parityInteger = SerialPort.PARITY_EVEN;
        }
        if (parity.equalsIgnoreCase("O")) {
            parityInteger = SerialPort.PARITY_ODD;
        }

        serialPort = new SerialPort(portId);
        try {
            serialPort.openPort();//Open serial port
            serialPort.setParams(baudRate, bits, stopBits, parityInteger); //Set params.
            serialPort.addEventListener(new SerialPortReader());

            systemLogArea.setText("Connected to " + portId + "@" + baudRate + System.getProperty("line.separator"));

        } catch (SerialPortException ex) {
            logger.error(ex.getMessage(), ex);
            systemLogArea.setText("Error connecting to port" + System.getProperty("line.separator"));
                // MOVE CURSOR WITH TEXT
                systemLogArea.setCaretPosition(systemLogArea.getDocument().getLength());
        }
    }

    /**
     *
     * Closes the serial port connection
     * 
     */
    public void closeConnection() {

        if (serialPort.isOpened()) {
            try {
                if (serialPort.closePort() == true) {
                    logAndDisplayInfoStatus("Disconnected from " + portIdentifier);
                } else {
                    logAndDisplayInfoStatus("Could not close serial port " + portIdentifier);
                }
            } catch (SerialPortException ex) {
                logger.error(ex.getMessage(), ex);
            }
        }
    }

    /**
     *
     * @param packetAsList
     * Writes to the serial port
     */
    private void serialPortWrite(List packetAsList) {

        logger.debug("------- SERIAL PORT WRITE START -------");
        logger.debug("serialPortWrite ---> " + packetAsList);
        logger.debug("------- SERIAL PORT WRITE END -------");

        try {
            serialPort.writeIntArray(Ints.toArray(packetAsList));
        } catch (SerialPortException ex) {
            logger.error(ex.getMessage(), ex);
        }

    }

    /**
     *
     * @param time
     * delay function
     * 
     */
    private void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

    /**
     *
     * @param data
     * Writes to the serial port and appends to the system log text area in the main window
     * 
     */
    public void sendCommand(String data) {
        try {
            serialPort.writeString(data + "\r");
            systemLogArea.append(data + System.getProperty("line.separator"));
            // MOVE CURSOR WITH TEXT
            systemLogArea.setCaretPosition(TrendConfig.systemLogArea.getDocument().getLength());
        } catch (SerialPortException ex) {
            java.util.logging.Logger.getLogger(SerialComms.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param data
     */
    public void sendIntArray(String data) {
        int x;
        try {
            if (data != null) {
                byte[] line = data.getBytes();
                serialPort.writeBytes(line);
            }
            systemLogArea.append("Modbus Command in full = " + data + System.getProperty("line.separator"));
            // MOVE CURSOR WITH TEXT
            systemLogArea.setCaretPosition(systemLogArea.getDocument().getLength());
        } catch (SerialPortException ex) {
            java.util.logging.Logger.getLogger(SerialComms.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param data
     */
    public void sendIntArray(int[] data) {
        int x;
        try {
            if (data != null) {
                serialPort.writeIntArray(data);
            }
            systemLogArea.append("Modbus Command in full = " + Arrays.toString(data) + System.getProperty("line.separator"));
            // MOVE CURSOR WITH TEXT
            systemLogArea.setCaretPosition(systemLogArea.getDocument().getLength());
        } catch (SerialPortException ex) {
            java.util.logging.Logger.getLogger(SerialComms.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param command
     * @param data
     * Method sends data the serial port and then then delays before continuing
     */
    public void userCommand(String command, String data) {
        int sleeptime = 1200;
        if (!"".equals(data)) {
            try {
                serialPort.writeString(command + data + "\r");
                Thread.sleep(sleeptime);
                printCommand(command, data);
            } catch (SerialPortException | InterruptedException ex) {
                java.util.logging.Logger.getLogger(SerialComms.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else if ("trend_address=".equals(command)) {
            try {
                serialPort.writeString("trend_address?" + "\r");
                Thread.sleep(sleeptime);
            } catch (SerialPortException | InterruptedException ex) {
                java.util.logging.Logger.getLogger(SerialComms.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     *
     * @param command
     * @param data
     * If a command is sent to the receiver, this method is then called which displays the data sent, unless the command is setting the Trend key.
     * 
     */
    public void printCommand(final String command, final String data) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                if (!"trend_key=".equals(command)) {
                    systemLogArea.append(command + data + System.getProperty("line.separator"));
                    systemLogArea.setCaretPosition(systemLogArea.getDocument().getLength());
                } else {
                    systemLogArea.append("Trend key" + System.getProperty("line.separator"));
                    // MOVE CURSOR WITH TEXT
                    systemLogArea.setCaretPosition(systemLogArea.getDocument().getLength());
                }
            }
        });
    }

    /**
     * This method is used to show info logs in the status area.
     *
     * @param info String
     */
    public static void logAndDisplayInfoStatus(String info) {

        logger.info(info);

        String timestampNow = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss").format(new Date(System.currentTimeMillis()));

        systemLogArea.append("[" + timestampNow + "] " + info + System.getProperty("line.separator"));
        // MOVE CURSOR WITH TEXT
        systemLogArea.setCaretPosition(systemLogArea.getDocument().getLength());

    }

    /**
     * Serial Port Reader Class
     *
     * Incoming data on the serial port is processed here
     *
     */
    class SerialPortReader implements SerialPortEventListener {

        StringBuilder message = new StringBuilder();
        Boolean receivingMessage = false;
        //Thread dataProcess = new Thread();

        public SerialPortReader() {
        }
        /**
         * Serial Event
         *
         * Serial data is received here and added to the preProcessingVector for
         * processing
         *
         *
         *
         * @param event
         */
        @Override
        public void serialEvent(SerialPortEvent event) {

            try {
                if (TrendConfig.command.getCommand() == TrendConfig.listNodes) {
                    Thread.sleep(1600); // WAIT FOR SERIAL PORT BUFFER TO FILL 
                } else {
                    Thread.sleep(150);
                }
            } catch (InterruptedException ex) {
                logger.error(ex.getMessage(), ex);
            }
            String[] arrayList = {};
            if (event.isRXCHAR()) {//If data is  available 
                if (event.getEventValue() > 0) {

                    //Read all data in buffer 
                    try {
                        arrayList = serialPort.readHexStringArray();
                        serialPort.purgePort(SerialPort.PURGE_RXCLEAR & SerialPort.PURGE_TXCLEAR);
                    } catch (SerialPortException ex) {
                        logger.error(ex.getMessage(), ex);
                    }
                }
            }
            addSerialDataToArrayList(arrayList);
        }

        /**
         * Add Serial Data to a List
         *
         * Add the array of hex string received from the serial port to the a
         * list and pass on to processing.
         *
         *
         * @param data
         *
         */
        private void addSerialDataToArrayList(String[] data) {

            List<String> packet = Arrays.asList(data);

            // PROCESS IF THERE IS ATLEASE ONE BYTE
            if (data.length > 0) {
                processSerialData(packet);
            }

        }

        /**
         *
         * @param s A hex string is converted to a byte array
         * @return
         */
        private byte[] hexStringToByteArray(String s) {
            int len = s.length();
            byte[] data = new byte[len / 2];
            for (int i = 0; i < len; i += 2) {
                data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                        + Character.digit(s.charAt(i + 1), 16));
            }
            return data;
        }

        /**
         * Process Serial Data
         *
         * Decode Serial Data packet after being processed by the
         * preProcessSerialData() method
         *
         *
         * @param data
         *
         */
        private void processSerialData(List<String> data) {

            logger.debug("data packet: <---" + data.toString());

            StringBuilder dataAsASCII = new StringBuilder();
            for (String data1 : data) {
                dataAsASCII.append(data1);
            }

            String ascii = new String(hexStringToByteArray(dataAsASCII.toString()));
            TrendConfig.logAndDisplayResponse(ascii, data.toString());
            logger.debug("ascii:" + ascii);

            //RESET ASCII
            ascii = "";
        }
    }

}
